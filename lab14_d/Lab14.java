import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by alex on 4.5.17.
 */
public class Lab15 {
    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int N = Integer.parseInt(reader.readLine());
            String[] str = reader.readLine().split(" ");
            int[] mas = new int[N + 2];
            for (int i = 0; i < N; i++) {
                mas[i + 1] = Integer.parseInt(str[i]);
                if (mas[i + 1] > mas[0]) mas[0] = mas[i + 1];
            }
            mas[N + 1] = mas[0];
            System.out.println(start(mas, N));
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }


    private static long start(int[] mas, int N) {
        int[] mas2 = new int[N + 2];
        for (int i = 0; i < N + 2; i++) mas2[i] = i;
        int a = 0;
        int b = 0;
        int c = 0;
        long count = 0;
        for (int i = 1; i < N + 1; i++) {
            if (mas[i] < mas[i + 1] && mas[i] < mas[N + 1]) {
                b = i;
                for (int j = i; j > 0; j--) {
                    j = mas2[j];
                    if (mas[j - 1] > mas[j]) {
                        a = j;
                        break;
                    }
                }
                if (mas[a - 1] < mas[b + 1]) c = mas[a - 1];
                else c = mas[b + 1];
                count += c - mas[a];
                mas[a] = c;
                mas[b] = c;
                mas2[b] = a;
                i = b - 1;
            }
        }
        return count;
    }
}