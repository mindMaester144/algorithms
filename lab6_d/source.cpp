#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

template<typename T = int *>
struct myComp {
    bool operator() (const T& h1, const T& h2) {
        return(h1[0] < h2[0]);
    }
};

int main() {
    int N;
    int M;
    int a;
    int b;
    int c;
    int mas;
    int count;
    int* elem;
    int* pA;
    int* A;
    priority_queue<int *, vector<int *>, myComp<>> queue;

    cin >> N;
    cin >> M;

    count = N;
    A = new int[N];
    pA = new int[N];
    for (int i = 0; i< N; i++) {
        pA[i] = -1;
        A[i] = 0;
    }

    for (int i = 0; i < M; i++) {
        cin >> a;
        cin >> b;
        cin >> c;
        queue.push(new int[3]{c, a - 1, b - 1});
    }

    while (!queue.empty()) {
        elem = queue.top();
        queue.pop();
        for (int j = elem[1]; j <= elem[2]; j++) {
            if (pA[j] == -1) {
                A[j] = elem[0];
                if (pA[elem[2]] == -1) pA[j] = elem[2];
                else pA[j] = pA[elem[2]];
                count--;
                if (count == 0) break;
            }
            else {
                j = pA[j];
                if (pA[j] < elem[2]) {
                    if (pA[elem[2]] == -1) pA[j] = elem[2];
                    else pA[j] = pA[elem[2]];
                }
            }
        }
    }

    for (int i = 0; i  < N; i++)
        cout << A[i] << " ";
    return 0;
}