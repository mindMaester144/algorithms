import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;
/**
 * Created by alex on 24.4.17.
 */
public class Lab16 {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new  BufferedReader(new InputStreamReader(System.in));
            String sX = reader.readLine();
            char[] sMas = sX.toCharArray();
            if (Integer.parseInt(sX) < 12) {
                System.out.println("-1");
                return;
            }
            int x;

            for (int i = sMas.length - 2; i >= 0; i--) {
                x = 0;
                for (int j = sMas.length - 1; j > i; j--) {
                    if (sMas[j] > sMas[i] && sMas[j] != '0' && (x == 0 || sMas[j] < sMas[x])) x = j;
                }
                if (x != 0 && x <= '9') {
                    char[] newMas = new char[sMas.length - (i + 1)];
                    char ch = sMas[x];
                    sMas[x] = sMas[i];
                    sMas[i] = ch;
                    for (int k = i + 1; k < sMas.length; k++) {
                        newMas[k - (i + 1)] = sMas[k];
                    }
                    Arrays.sort(newMas);
                    for (int k = i + 1; k < sMas.length; k++) {
                        sMas[k] = newMas[k - (1 + i)];
                    }
                    System.out.println(String.valueOf(sMas));
                    return;
                }
            }
            System.out.println("-1");
        }
        catch (IOException e) {
            System.out.println("-1");
        }
    }
}