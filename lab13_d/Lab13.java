import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by alex on 25.4.17.
 */
public class Lab13 {
    private static int[] p;
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String[] str = reader.readLine().split(" ");
            int N = Integer.parseInt(str[0]);
            int M = Integer.parseInt(str[1]);
            int count = 0;
            p = new int[N];
            for (int i = 0; i < N; i++) p[i] = i;
            for (int i = 0; i < M; i++) {
                str = reader.readLine().split(" ");
                if (unite(Integer.parseInt(str[0]) - 1, Integer.parseInt(str[1]) - 1)) count++;
            }
            if (count < N - 1) System.out.println("-1");
            else System.out.println(M - count);
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private static boolean unite(int a, int b) {
        a = find(a);
        b = find(b);
        if (a == b) return false;
        if (new Random().nextInt(2) == 0) p[a] = b;
        else p[b] = a;
        return true;
    }

    private static int find(int x) {
        if (p[x] == x) return x;
        return p[x] = find(p[x]);
    }
}