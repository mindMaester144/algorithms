import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lab4 {
    private static int minSteps = -1;
    private static int i;
    private static int j;
    private static int n;
    private static int m;
    private static int[][] map;

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String stringMas[] = reader.readLine().split(" ");
            n = Integer.parseInt(stringMas[0]);//1 ≤ n, m ≤ 100, 1 ≤ i ≤ n, 1 ≤ j ≤ m
            m = Integer.parseInt(stringMas[1]);//n, m - размеры;
            i = Integer.parseInt(stringMas[2]);//3 3 2 2 = never
            j = Integer.parseInt(stringMas[3]);//15 79 13 47 = 24
            map = new int[n][m];
            find(1, 1, -1);
            if (minSteps == -1) System.out.println("NEVAR");
            else System.out.println(minSteps);
        } catch (Exception e) {System.out.print(e);}
    }

    private static void find(int x, int y, int step) {
        step++;
        if (map[x - 1][y - 1] > 0 && map[x - 1][y - 1] < step) return;
        else map[x - 1][y - 1] = step;
        if(minSteps >= 0 && step >= minSteps) return;
        if(x == i && y == j) {
            if(minSteps < 0 || step < minSteps) {
                minSteps = step;
            }
            return;
        }
        if (x + 15 < i || y + 15 < j) {
            if (x + 15 < i && y + 15 < j) {
                if (i - x > j - y) find(x + 2, y + 1, step);
                else find(x + 1, y + 2, step);
                return;
            }
            if (x + 15 < i) {
                if (y > j) {
                    find(x + 2, y - 1, step);
                }
                else if (y < j){
                    find(x + 2, y + 1, step);
                }
                else {
                    if (y + 1 <= m) find(x + 2, y + 1, step);
                    else find(x + 2, y - 1, step);
                }
                return;
            }
            if (y + 15 < j) {
                if (x > i) {
                    find(x - 1, y + 2, step);
                }
                else if (x < i) {
                    find(x + 1, y + 2, step);
                }
                else {
                    if (x + 1 <= n) find(x + 1, y + 2, step);
                    else find(x - 1, y + 2, step);
                }
            }
        }
        else {
            if (x <= i) {
                if (x + 1 <= n) {
                    if (y <= j && y + 2 <= m) find(x + 1, y + 2, step);
                    if (y >= j && y - 2 >= 1) find(x + 1, y - 2, step);
                }
                if (x + 2 <= n) {
                    if (y <= j && y + 1 <= m) find(x + 2, y + 1, step);
                    if (y >= j && y - 1 >= 1) find(x + 2, y - 1, step);
                }
            }
            if (x >= i) {
                if (x - 2 >= 1) {
                    if (y <= j && y + 1 <= m) find(x - 2, y + 1, step);
                    if (y >= j && y - 1 >= 1) find(x - 2, y - 1, step);
                }
                if (x - 1 >= 1) {
                    if (y <= j && y + 2 <= m) find(x - 1, y + 2, step);
                    if (y >= j && y - 2 >= 1) find(x - 1, y - 2, step);
                }
            }
        }
    }
}