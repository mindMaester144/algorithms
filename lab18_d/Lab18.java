import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by alex on 24.4.17.
 */
public class Lab18 {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int year = Integer.parseInt(reader.readLine());

            if ((year % 4 == 0 && !(year % 100 == 0)) || (year % 400 == 0)) {
                System.out.println("YES");
            }
            else {
                System.out.println("NO");
            }
        }
        catch (Exception e) {System.out.println("Exception");}
    }
}