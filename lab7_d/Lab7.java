import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by alex on 24.4.17.
 */
public class Lab7 {
    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String[] strings = reader.readLine().split(" ");
            if (Math.abs(Integer.parseInt(strings[2]) - Integer.parseInt(strings[4])) ==
                    Math.abs(Integer.parseInt(strings[3]) - Integer.parseInt(strings[5]))) {
                System.out.println("NO");
            }
            else {
                System.out.println("YES");
            }
        }catch (Exception e) {}
    }
}