import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Lab2 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int N = Integer.parseInt(reader.readLine());
            String stringMas[] = reader.readLine().split(" ");
            int firstPos = Integer.valueOf(-1);
            int secondPos = Integer.valueOf(-1);
            int firstNeg = Integer.valueOf(1);
            int secondNeg = Integer.valueOf(1);
            int x;
            for (int i = 0; i < N; i++) {
                x = Integer.parseInt(stringMas[i]);
                if (x > secondPos) {
                    if (x > firstPos) {
                        secondPos = firstPos;
                        firstPos = x;
                    }
                    else {
                        secondPos = x;
                    }
                }
                else {
                    if (x < secondNeg) {
                        if (x < firstNeg) {
                            secondNeg = firstNeg;
                            firstNeg = x;
                        }
                        else {
                            secondNeg = x;
                        }
                    }
                }
            }
            if (firstNeg > 0) {
                System.out.println((long)firstPos * secondPos);
            }
            else if (firstPos < 0) {
                System.out.println((long)firstNeg * secondNeg);
            }
            else if (secondNeg > 0 && secondPos < 0) {
                System.out.println((long)firstNeg * firstPos);
            }
            else if (secondNeg > 0) {
                System.out.println((long)firstPos * secondPos);
            }
            else if (secondPos < 0) {
                System.out.println((long)firstNeg * secondNeg);
            }
            else {
                System.out.println((long)firstPos * secondPos > (long)firstNeg * secondNeg ? (long)firstPos * secondPos : (long)firstNeg * secondNeg);
            }
        } catch (Exception e) {}
    }
}