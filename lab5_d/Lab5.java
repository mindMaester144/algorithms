import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * Created by alex on 25.4.17.
 */
public class Lab6 {
    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String[] str = reader.readLine().split(" ");
            int N = Integer.parseInt(str[0]);
            int M = Integer.parseInt(str[1]);
            PriorityQueue<int[]> queue = new PriorityQueue<>(new Comparator<int[]>() {
                @Override
                public int compare(int[] o1, int[] o2) {
                    return o2[2] - o1[2];
                }
            });
            for (int i = 1; i <= M; i++) {
                str = reader.readLine().split(" ");
                queue.add(new int[] {
                        Integer.parseInt(str[0]) - 1,
                        Integer.parseInt(str[1]) - 1,
                        Integer.parseInt(str[2]),
                });
            }
            int[] A = start(queue, N, M);
            for (int i = 0; i < N; i++) {
                System.out.print(A[i] + " ");
            }
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private static int[] start(PriorityQueue queue, int N, int M) {
        int count = N;
        int[] A = new int[N];
        int[] pA = new int[N];
        for (int i = 0; i < N; i++) pA[i] = -1;
        int[] elem;

        for(int q = 0; q < M; q++) {
            elem = (int[])queue.poll();
            for (int i = elem[0]; i <= elem[1]; i++) {
                if (pA[i] == -1) {
                    A[i] = elem[2];
                    if (pA[elem[1]] == -1) pA[i] = elem[1];
                    else pA[i] = pA[elem[1]];
                    count--;
                    if (count == 0) {
                        break;
                    }
                }
                else {
                    i = pA[i];
                    if (pA[i] < elem[1]) {
                        if (pA[elem[1]] == -1) pA[i] = elem[1];
                        else pA[i] = pA[elem[1]];
                    }
                }
            }
        }
        return A;
    }
}
