import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by alex on 25.4.17.
 */
public class Lab8 {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String str = reader.readLine();
            System.out.println(find(str));
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private static int find(String str) {
        char[] sMas = str.toCharArray();
        if (sMas.length < 2) return -1;
        if (sMas.length == 2) {
            if (sMas[0] == sMas[1]) return -1;
            else return 2;
        }
        boolean isSame = true;
        for (int i = 0; i < sMas.length / 2 + sMas.length % 2; i++) {
            if (sMas[i] != sMas[sMas.length - 1 - i]) {
                return sMas.length;
            }
            if (sMas[0] != sMas[i] || sMas[0] != sMas[sMas.length - 1 - i]) isSame = false;
        }
        if (isSame) return -1;
        return sMas.length - 1;
    }
}
