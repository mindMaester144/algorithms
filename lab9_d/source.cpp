#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    string str;
    int a;
    int b;
    int Q;
    bool * mas;
    bool isChange = false;

    do {
        cin >> str;
    } while(!all_of(str.begin(), str.end(), [](char x){return isalpha(x);}));

    cin >> Q;

    mas = new bool[str.length() + 1];
	for (int i = 0; i < str.length() + 1 ; i++) mas[i] = false;

    for (int i = 0; i < Q; i++) {
        cin >> a;
        cin >> b;
        if (a > b)
            swap(a, b);
        mas[a - 1] = !mas[a - 1];
        mas[b] = !mas[b];
    }


    for (int i = 0; i < str.length(); i++) {
        if (mas[i]) isChange = !isChange;
        if (isChange) {
            if (isupper(str[i]))
                str[i] = tolower(str[i]);
            else
                str[i] = toupper(str[i]);
        }
    }

    cout << str;
    return 0;
}