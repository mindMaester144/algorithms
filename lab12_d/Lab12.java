import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by alex on 25.4.17.
 */
public class Lab12 {
    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            long X = Long.parseLong(reader.readLine());
            if (X % 10 != 0) System.out.println(X % 10);
            else System.out.println("NO");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
