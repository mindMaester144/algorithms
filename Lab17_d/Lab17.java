import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by alex on 24.4.17.
 */
public class Lab17 {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int N = Integer.parseInt(reader.readLine());
            boolean isNumb1 = false;
            boolean isNumb2 = false;
            boolean isNumb3 = false;
            int numb1 = 1021;
            int numb2 = 1031;
            int numb3 = 1033;
            int numb12 = numb1 * numb2;
            int numb13 = numb1 * numb3;
            int numb23 = numb2 * numb3;
            int numb123 = numb1 * numb2 * numb3;
            int numb;
            for(int i = 0; i < N; i++) {
                numb = Integer.parseInt(reader.readLine());
                if (numb == numb1) isNumb1 = true;
                if (numb == numb2) isNumb2 = true;
                if (numb == numb3) isNumb3 = true;
                if (numb == numb12) {
                    isNumb1 = true;
                    isNumb2 = true;
                }
                if (numb == numb13) {
                    isNumb1 = true;
                    isNumb3 = true;
                }
                if (numb == numb23) {
                    isNumb2 = true;
                    isNumb3 = true;
                }
                if (numb == numb123) {
                    isNumb1 = true;
                    isNumb2 = true;
                    isNumb3 = true;
                }
            }
            if (isNumb1 && isNumb2 && isNumb3) System.out.println("YES");
            else System.out.println("NO");
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
