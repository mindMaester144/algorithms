import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by alex on 6.5.17.
 */
public class Lab20_test {
    private static int[][] A;
    private static byte[][] pA;
    private static int N;
    private static int M;
    private static int countOfWater;
    private static int lowestWall;
    private static int xNextToLW;
    private static int yNextToLW;


    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int T = Integer.parseInt(reader.readLine());
            String[] str;
            A = new int[50][50];
            pA = new byte[50][50];
            for (int i = 0; i < T; i++) {
                str = reader.readLine().split(" ");
                N = Integer.parseInt(str[0]);
                M = Integer.parseInt(str[1]);
                for (int j = 0; j < N; j++) {
                    str = reader.readLine().split(" ");
                    for (int k = 0; k < M; k++) {
                        A[j][k] = Integer.parseInt(str[k]);
                    }
                }
                System.out.println(start());
            }
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private static int start() {
        for (int j = 0; j < N; j++) {
            for (int k = 0; k < M; k++) {
                pA[j][k] = 1;
            }
        }
        countOfWater = 0;
        parsePA1();
        parsePA2();
        return countOfWater;
    }

    private static void parsePA1() {
        for (int i = 0; i < N ; i++) {
            pA[i][0] = 0;
            pA[i][M - 1] = 0;
        }
        for (int i = 0; i < M; i++) {
            pA[0][i] = 0;
            pA[N - 1][i] = 0;
        }
        for (int i = 1; i < N - 1; i++) {
            parsePA1Next(i, 0);
            parsePA1Next(i, M - 1);
        }
        for (int i = 1; i < M - 1; i++) {
            parsePA1Next(0, i);
            parsePA1Next(N - 1, i);
        }
    }

    private static void parsePA1Next(int x, int y) {
        if (x + 1 < N - 1 && pA[x + 1][y] != 0 && A[x + 1][y] >= A[x][y]) {
            pA[x + 1][y] = 0;
            parsePA1Next(x + 1, y);
        }
        if (x - 1 > 0 && pA[x - 1][y] != 0 && A[x - 1][y] >= A[x][y]) {
            pA[x - 1][y] = 0;
            parsePA1Next(x - 1, y);
        }
        if (y + 1 < M - 1 && pA[x][y + 1] != 0 && A[x][y + 1] >= A[x][y]) {
            pA[x][y + 1] = 0;
            parsePA1Next(x, y + 1);
        }
        if (y - 1 > 0 && pA[x][y - 1] != 0 && A[x][y - 1] >= A[x][y]) {
            pA[x][y - 1] = 0;
            parsePA1Next(x, y - 1);
        }
    }

    private static void parsePA2() {
        for (int i = 1; i < N - 1; i++) {
            for (int j = 1; j < M - 1; j++) {
                if (pA[i][j] == 2) makeUnknownNext(i, j);
                if (pA[i][j] == 1) {
                    while (pA[i][j] != 0) {
                        if (pA[i][j] == 2) makeUnknownNext(i, j);
                        lowestWall = 10001;
                        findLWNext(i, j);
                        parsePA2Next(xNextToLW, yNextToLW);
                    }
                }
            }
        }
    }

    private static void parsePA2Next(int x, int y) {
        pA[x][y] = 0;
        countOfWater += lowestWall - A[x][y];
        A[x][y] = lowestWall;
        if (pA[x + 1][y] == 2) {
            if (A[x + 1][y] < lowestWall) {
                parsePA2Next(x + 1, y);
            }
            else {
                pA[x + 1][y] = 0;
                parsePA1Next(x + 1, y);
            }
        }

        if (pA[x - 1][y] == 2) {
            if (A[x - 1][y] < lowestWall) {
                parsePA2Next(x - 1, y);
            }
            else {
                pA[x - 1][y] = 0;
                parsePA1Next(x - 1, y);
            }
        }

        if (pA[x][y + 1] == 2) {
            if (A[x][y + 1] < lowestWall) {
                parsePA2Next(x, y + 1);
            }
            else {
                pA[x][y + 1] = 0;
                parsePA1Next(x, y + 1);
            }
        }

        if (pA[x][y - 1] == 2) {
            if (A[x][y - 1] < lowestWall) {
                parsePA2Next(x, y - 1);
            }
            else {
                pA[x][y - 1] = 0;
                parsePA1Next(x, y - 1);
            }
        }
    }

    private static void findLWNext(int x, int y) {
        pA[x][y] = 2;
        if (pA[x + 1][y] == 1) {
            findLWNext(x + 1, y);
        }
        else if (pA[x + 1][y] == 0 && A[x + 1][y]  < lowestWall) {
            lowestWall = A[x + 1][y];
            xNextToLW = x;
            yNextToLW = y;
        }

        if (pA[x - 1][y] == 1) {
            findLWNext(x - 1, y);
        }
        else if (pA[x - 1][y] == 0 && A[x - 1][y]  < lowestWall) {
            lowestWall = A[x - 1][y];
            xNextToLW = x;
            yNextToLW = y;
        }

        if (pA[x][y + 1] == 1) {
            findLWNext(x, y + 1);
        }
        else if (pA[x][y + 1] == 0 && A[x][y + 1]  < lowestWall) {
            lowestWall = A[x][y + 1];
            xNextToLW = x;
            yNextToLW = y;
        }

        if (pA[x][y - 1] == 1) {
            findLWNext(x, y - 1);
        }
        else if (pA[x][y - 1] == 0 && A[x][y - 1]  < lowestWall) {
            lowestWall = A[x][y - 1];
            xNextToLW = x;
            yNextToLW = y;
        }
    }

    private static void makeUnknownNext(int x, int y){
        pA[x][y] = 1;
        if (pA[x + 1][y] == 2) {
            makeUnknownNext(x + 1, y);
        }
        if (pA[x - 1][y] == 2) {
            makeUnknownNext(x - 1, y);
        }
        if (pA[x][y + 1] == 2) {
            makeUnknownNext(x, y + 1);
        }
        if (pA[x][y - 1] == 2) {
            makeUnknownNext(x, y - 1);
        }
    }
}
