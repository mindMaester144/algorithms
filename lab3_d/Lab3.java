import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by alex on 28.4.17.
 */
public class Lab3_1 {

    public static void main(String[] args) {
        startLab();
    }

    private static void startLab() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String[] str = reader.readLine().split(" ");
            int N = Integer.parseInt(str[0]);
            int K = Integer.parseInt(str[1]);
            str = reader.readLine().split(" ");
            int[] A = new int[N];
            for (int i = 0; i < N; i++) {
                A[i] = Integer.parseInt(str[i]);
            }
            System.out.println(start(A, N, K));
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private static long start(int[] A, int N, int K) {
        Arrays.sort(A);
        int left = 0;
        int right = 0;
        if (K % 2 == 1 && A[N - 1] < 0) {
            right = K;
        }
        else {
            if (K % 2 == 1) right++;
            for (int i = 0; i < K / 2; i++) {
                if ((long)A[left] * (long)A[left + 1] > (long)A[(N - 1) - right] * (long)A[(N - 1) - (right + 1)]) {
                    left += 2;
                } else {
                    right += 2;
                }
            }
        }
        long result = 1;
        for (int i = 0; i < left; i++) {
            result *= A[i];
            result %= 1000000007;
            if (result < 0) result += 1000000007;

        }
        for (int i = 0; i < right; i++) {
            result *= A[(N - 1) - i];
            result %= 1000000007;
            if (result < 0) result += 1000000007;

        }
        return result;
    }
}