import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Lab1 {
    private static int[] mas;
    private static Random random;

    public static boolean unite(int a, int b) {
        a = find(a);
        b = find(b);
        if (a == b) return false;
        if (random.nextInt(2) == 0) mas[a] = b;
        else mas[b] = a;
        return true;
    }

    public static int find(int x) {
        if (mas[x] == x) return x;
        return mas[x] = find(mas[x]);
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        random = new Random();
        try {
            String[] stringMas = reader.readLine().split(" ");
            int N = Integer.parseInt(stringMas[0]); // кол-во вершин
            int M = Integer.parseInt(stringMas[1]); // кол-во ребер

            mas = new int[N];
            for (int i = 0; i < N; i++) mas[i] = i;
            for (int i = 0; i < M; i++) {
                stringMas = reader.readLine().split(" ");
                if (unite(Integer.parseInt(stringMas[0]) - 1, Integer.parseInt(stringMas[1]) - 1)) N--;
            }

            System.out.println((N - 1));
        } catch (Exception e) {System.out.println(e);}
    }
}
